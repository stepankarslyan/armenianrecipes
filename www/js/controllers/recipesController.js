starter.controller('RecipesCtrl', function($scope) {
	$scope.playlists = [
		{
			id: 1, 
			category: 'salad', 
			title: "Armenian lahmajo", 
			description: "Description of this food", 
			ingredients: "1/3 spoon of oil", 
			direction: "Directions for lahmajo", 
			likes: 11
		}
	];
});