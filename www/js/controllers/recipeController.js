starter.controller('RecipeCtrl', function($scope, $ionicActionSheet, $timeout) {
	$scope.recipe = {
		id: 1, 
		category: 'salad', 
		title: "Armenian lahmajo", 
		description: "Description of this food", 
		ingredients: "1/3 spoon of oil", 
		direction: "Directions for lahmajo", 
		likes: 11
	};

	// Triggered on a button click, or some other target
	$scope.show = function() {

	   // Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			buttons: [
				{ text: '<b>Share</b> This' },
				{ text: 'Move' }
			],
			destructiveText: 'Delete',
			titleText: 'Modify your album',
			cancelText: 'Cancel',
			cancel: function() {
			  $ionicActionSheet.hide();
			},
			buttonClicked: function(index) {
				return true;
			}
	     });

	};
});